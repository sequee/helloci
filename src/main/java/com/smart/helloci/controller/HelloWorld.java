package com.smart.helloci.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "hello")
@RequestMapping("/hello")
@RestController
public class HelloWorld {

    @ApiOperation("hello-ci")
    @GetMapping("/ci")
    public String greeting() {
        System.out.println("hello");
        return "hello-ci";
    }
}
