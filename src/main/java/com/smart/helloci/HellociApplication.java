package com.smart.helloci;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableSwagger2Doc
@ComponentScan("com.smart.helloci")
public class HellociApplication {

    public static void main(String[] args) {
        SpringApplication.run(HellociApplication.class, args);
    }

}
