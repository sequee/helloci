FROM anapsix/alpine-java
COPY ./target/helloci-0.0.1-SNAPSHOT.jar /sxdata/helloci/
WORKDIR /sxdata/helloci/
ENV PARAMS=""
ENTRYPOINT ["sh","-c","java $PARAMS -jar helloci-0.0.1-SNAPSHOT.jar"]
